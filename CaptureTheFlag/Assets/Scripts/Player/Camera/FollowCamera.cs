﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour
{
	public float minDistance;
	public float maxDistance;
	public float smooth;
	private Vector3 _dollyDir;
	private float _distance; 
	
	public Transform playerBody;

	private float xRotation = 0f;
	void Awake() //renvoie la distance et la direction de la camera
	{
		_dollyDir = transform.localPosition.normalized;
		_distance = transform.localPosition.magnitude;
	}
	void Start() // Lock the mouse 
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}
	void Update() // Envoie un raycast vers le centre, si le raycast touche un objet alors la camera avance
	{
		Vector3 desiredCameraPos = transform.parent.TransformPoint(_dollyDir * maxDistance);
		RaycastHit hit;
		if( Physics.Linecast( transform.parent.position, desiredCameraPos, out hit ) )
		{
			_distance = Mathf.Clamp( hit.distance, minDistance, maxDistance );
		}
		else
		{
			_distance = maxDistance;
		}
		transform.localPosition=Vector3.Lerp(transform.localPosition, _dollyDir * _distance, Time.deltaTime * smooth); 
		
		if (Input.GetKey(KeyCode.Q))
		{
			//transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
			playerBody.Rotate(Vector3.up);
		}
		if (Input.GetKey(KeyCode.D))
		{
			//transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
			playerBody.Rotate(Vector3.down);
		}
	}

	
}